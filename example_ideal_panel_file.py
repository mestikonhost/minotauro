'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

from model.minotauro import PanelPV


def run():
    panel = PanelPV(Vi=0,
                    s=1,
                    p=1,
                    Ei=1000,
                    Ein=1000,
                    Tn=25,
                    b=0.0684,
                    Isc=3.71,
                    Voc=21.40,
                    T=25,
                    TCv=-0.1261,
                    TCi=0.00418)

    step = 0.01

    Vi, Iv, Pot = panel.curve_PV_IV(step)

    with open('output_ideal_panel.dat', 'w') as f:
        for i in range(len(Vi)):
            f.write('{voltaje:>10.6f} {corriente:>10.6f} {potencia:>10.6f}\n'.format(
                voltaje=Vi[i], corriente=Iv[i], potencia=Pot[i]))


if __name__ == '__main__':
    run()
