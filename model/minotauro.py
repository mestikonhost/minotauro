'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''


import numpy as np


class PanelPV:
    def __init__(self, Vi, s, p, Ei, Ein, Tn, b, Isc, Voc, T, TCv, TCi):
        self.Vi = Vi
        self.s = s
        self.p = p
        self.Ei = Ei
        self.Ein = Ein
        self.Tn = Tn
        self.b = b
        self.Isc = Isc
        self.Voc = Voc
        self.T = T
        self.TCv = TCv
        self.TCi = TCi
        self.Vmax = self.Voc*1.03
        self.Vmin = self.Voc*0.5

    def corriente(self):
        self.Ix = self.p*(self.Ei/self.Ein)*(self.Isc +
                                             (self.TCi*(self.T - self.Tn)))
        return self.Ix

    def voltaje(self):
        self.Vx = self.s*(self.Ei/self.Ein)*self.TCv*(self.T-self.Tn) + (self.s*self.Vmax) - (self.s*(
            self.Vmax - self.Vmin))*np.exp((self.Ei/self.Ein)*np.log((self.Vmax - self.Voc)/(self.Vmax - self.Vmin)))
        return self.Vx

    def OrtizModel_init(self):
        PanelPV.corriente(self)
        PanelPV.voltaje(self)

    def OrtizCorriente(self):
        self.Iv = (self.Ix/(1 - np.exp((-1/self.b)))) * \
            (1-(np.exp((self.Vi/(self.b*self.Vx)) - (1/self.b))))
        return self.Iv

    def OrtizPotencia(self):
        self.Pot = PanelPV.OrtizCorriente(self)*self.Vi
        return self.Pot

    def curve_PV_IV(self, step):
        self.Vi = step
        self.finc = PanelPV.voltaje(self)/step
        PanelPV.OrtizModel_init(self)

        self.list_Vi = []
        self.list_Iv = []
        self.list_Pot = []

        for i in range(int(self.finc)):
            self.Iv = PanelPV.OrtizCorriente(self)
            self.Pot = PanelPV.OrtizPotencia(self)

            self.list_Vi.append(self.Vi)
            self.list_Iv.append(self.Iv)
            self.list_Pot.append(self.Pot)

            self.Vi = self.Vi + step

        return self.list_Vi, self.list_Iv, self.list_Pot


class Turbine:
    def __init__(self, cp, rho, area, velwind, c1, c2, c3, c4, c5, c6, lambda0, beta, lambdai):
        self.cp = cp
        self.rho = rho
        self.area = area
        self.velwind = velwind
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.c4 = c4
        self.c5 = c5
        self.c6 = c6
        self.lambda0 = lambda0
        self.beta = beta
        self.lambdai = lambdai

    def power(self):
        self.Pot = 0.5*self.cp*self.rho*self.area*np.power(self.velwind, 3)
        return self.Pot

    def lambdaiter(self):
        self.x1 = 1/(self.lambda0 + 0.08*self.beta)
        self.x2 = 0.035/(np.power(self.beta, 3) + 1)
        self.x3 = 1/(self.x1 - self.x2)
        return self.x3

    def power_coefficient(self):
        self.cp = self.c1*((self.c2/self.lambdai) - self.c3*self.beta -
                           self.c4)*(np.exp(-self.c5/self.lambdai)) + self.c6*self.lambda0
        return self.cp

    def find_cp(self, step):
        self.list_lambda0 = []
        self.list_cp = []

        for i in range(step):
            self.lambda0 = self.lambda0 + 1
            self.lambdai = Turbine.lambdaiter(self)
            self.cp = Turbine.power_coefficient(self)

            self.list_lambda0.append(self.lambda0)
            self.list_cp.append(self.cp)

        return self.list_lambda0, self.list_cp

    def curve_power(self, step, limit):
        self.list_Pot = []
        self.list_velwind = []

        for i in range(step):
            if self.velwind >= limit:
                self.velwind = limit

            self.list_Pot.append(Turbine.power(self))
            self.list_velwind.append(i)

            self.velwind = self.velwind + 1

        self.list_Pot.append(0)
        self.list_velwind.append(step + 0.25)

        return self.list_velwind, self.list_Pot
