'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

import matplotlib.pyplot as plt
import numpy as np
from model.minotauro import PanelPV


def _mayor(x1, x2, x3, x4, x5):
    y1 = len(x1)
    y2 = len(x2)
    y3 = len(x3)
    y4 = len(x4)
    y5 = len(x5)

    if y1 > y2 and y1 > y3 and y1 > y4 and y1 > y5:
        return x1
    elif y2 > y1 and y2 > y3 and y2 > y4 and y2 > y5:
        return x2
    elif y3 > y1 and y3 > y2 and y3 > y4 and y3 > y5:
        return x3
    elif y4 > y1 and y4 > y2 and y4 > y3 and y4 > y5:
        return x4
    elif y5 > y1 and y5 > y2 and y5 > y3 and y5 > y4:
        return x5


def _complete(size, list):
    if size > len(list):
        dif = size - len(list)
        comp = [0 for i in range(dif)]
        comp = np.array(comp)

        return np.concatenate((list, comp), axis=0)
    else:
        return list


def run():
    panel1 = PanelPV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25,
                     b=0.0684, Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel2 = PanelPV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25,
                     b=0.0684, Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel3 = PanelPV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25,
                     b=0.0684, Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel4 = PanelPV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25,
                     b=0.0684, Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)
    panel5 = PanelPV(Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25,
                     b=0.0684, Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418)

    step = 0.01

    panel1.Ei = 10
    panel1.T = 5

    panel2.Ei = 100
    panel2.T = 45

    panel3.Ei = 100
    panel3.T = 10

    panel4.Ei = 500
    panel4.T = 150

    panel5.Ei = 700
    panel5.T = 150

    Vi1, Iv1, Pot1 = panel1.curve_PV_IV(step)
    Vi2, Iv2, Pot2 = panel2.curve_PV_IV(step)
    Vi3, Iv3, Pot3 = panel3.curve_PV_IV(step)
    Vi4, Iv4, Pot4 = panel4.curve_PV_IV(step)
    Vi5, Iv5, Pot5 = panel5.curve_PV_IV(step)

    Vi1 = np.array(Vi1)
    Vi2 = np.array(Vi2)
    Vi3 = np.array(Vi3)
    Vi4 = np.array(Vi4)
    Vi5 = np.array(Vi5)

    Pot1 = np.array(Pot1)
    Pot2 = np.array(Pot2)
    Pot3 = np.array(Pot3)
    Pot4 = np.array(Pot4)
    Pot5 = np.array(Pot5)

    Vi = _mayor(Vi1, Vi2, Vi3, Vi4, Vi5)
    Pot = _mayor(Pot1, Pot2, Pot3, Pot4, Pot5)
    size = len(Pot)

    Pot1x = _complete(size, Pot1)
    Pot2x = _complete(size, Pot2)
    Pot3x = _complete(size, Pot3)
    Pot4x = _complete(size, Pot4)
    Pot5x = _complete(size, Pot5)

    Pot = Pot1x + Pot2x + Pot3x + Pot4x + Pot5x

    plt.plot(Vi1, Pot1)
    plt.plot(Vi2, Pot2)
    plt.plot(Vi3, Pot3)
    plt.plot(Vi4, Pot4)
    plt.plot(Vi5, Pot5)
    plt.plot(Vi, Pot)
    plt.show()


if __name__ == '__main__':
    run()
