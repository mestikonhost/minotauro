# Minotauro Project

Modeling of all the elements that make up a hybrid solar-wind system, integration of its components and 
some control algorithms.

>>>
##### Note 1: 
This script makes use of the numpy module, matplotlib and plotext.
>>>

>>>
##### Note 2:
All scripts are in the pep8 format:

autopep8 -i file.py
>>>
