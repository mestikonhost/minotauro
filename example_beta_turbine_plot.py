'''
* ------------------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
* ------------------------------------------------------------------------------------
'''

import matplotlib.pyplot as plt
from model.minotauro import Turbine


def run():
    turbine = Turbine(cp=0.1,
                      rho=1.225,
                      area=1.0751,
                      velwind=0.0,
                      c1=0.5176,
                      c2=116,
                      c3=0.4,
                      c4=5,
                      c5=21,
                      c6=0.0068,
                      lambda0=0.0,
                      beta=0.0,
                      lambdai=1.0)
    step = 15

    list_lambda0 = []
    list_cp = []

    for turbine.beta in range(0, 25, 5):
        lambda0, cp = turbine.find_cp(step)
        turbine.lambda0 = 0.0
        list_lambda0.append(lambda0)
        list_cp.append(cp)

    plt.plot(list_lambda0[0], list_cp[0])
    plt.plot(list_lambda0[1], list_cp[1])
    plt.plot(list_lambda0[2], list_cp[2])
    plt.plot(list_lambda0[3], list_cp[3])
    plt.plot(list_lambda0[4], list_cp[4])
    plt.show()


if __name__ == '__main__':
    run()
